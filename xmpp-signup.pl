#!/usr/bin/env perl

use 5.010;
use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use Net::XMPP qw/ Client /;
use Term::ReadKey;

my $host;
my $jid;
my $pass;

GetOptions(
	'h=s' => \$host,
	'j=s' => \$jid,
	'p=s' => \$pass
);

$host && $jid || pod2usage(1);

my $con = new Net::XMPP::Client();
if (!$con->Connect('hostname' => $host, 'tls' => 1)) {
	print "Connection error\n";
	exit 1;
}
if (!$pass) {
	ReadMode('noecho');
	print "Password: ";
	$pass = <STDIN>;
	ReadMode(0);
	print "\n";
	chomp($pass);
}
my @rv = $con->RegisterSend(
	'to' => $host,
	'username' => $jid,
	'password' => $pass);
if ($rv[0] ne "ok") {
	print "Failed to register. Already registered? " . $rv[1] . "\n";
	$con->Disconnect();
	exit 1;
}
print "Successfuly registered ${jid} at ${host}.\n";
$con->Disconnect();

__END__

=head1 NAME

xmpp-signup - register account on XMPP server

=head1 SYNOPSIS

xmpp-signup -h <host> -j <jid> [-p <pass>]

=head1 OPTIONS

=over 8

=item B<-h>

Service hostname.

=item B<-j>

Jabber ID.

=item B<-p>

Password. If not given, requested interactively.

=back

=head1 DESCRIPTION

B<This program> registers user account on XMPP service.

=cut
